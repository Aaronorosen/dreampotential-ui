import React, { useRef, useState } from 'react';
import '../App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
// import {useHistory} from 'react-router-dom'
import { BrowserRouter, Routes, Route, useNavigate } from "react-router-dom";
import NavbarSection from '../Components/Navbar/Navbar';
import Hero from '../Components/HeroSection/Hero';
import Middle from '../Components/MiddleSection/Middle';
import Select_Partners from '../Components/Select_Partners/Select_Partners';
import GetInTouch from '../Components/GetInTouch/GetInTouch';
import Footer from '../Components/Footer/Footer';
import NewHero from '../Components/NewHeroSection/NewHero';
import { toasts, ToastContainer, FlatToast } from 'svelte-toasts';


const Login = function(){

 const [email, setEmail] = useState();
 const [password, setPassword] = useState();
 const navigate = useNavigate();


 async function handleLogin() {

    try {
        // Send a POST request to your dummy API for login using the fetch API
        console.log('Login button has clicked');
        const user = { email, password };
        const jsonString = JSON.stringify(user);
        console.log('jsonString in login page', jsonString);

        const response = await fetch('http://app.realtorstat.com:8021/usersystem/user/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: jsonString
        });
        console.log('response from login page', response);
        const data = await response.json();
        console.log('data from login page', data);

        if (response.ok) {
            // Example: navigate('/dashboard');
            console.log('res', response);
            localStorage.setItem('session_id', JSON.stringify(data));
            sessionStorage.setItem('session_id', JSON.stringify(data));
            console.log('Login successful');
            toasts.success('LoggedIn Successfully', { placement: 'top-right', theme: 'light' });
           navigate('/');

        } else {
            const responseData = await response.json();
            console.error('Login failed:', responseData.message);
        }
    } catch (error) {
        console.error('An error occurred:', error);
    }

   

   }


return (
    <div className="newContainer">
    <div className="signup-form">
    <div className='form1'>
   <h4>Sign In to MeylorCI</h4>
		<p className="hint-text">New Here? <a href="/signup" >Sign up</a> </p>
        <div className="form-group">
			<div className="row">
				<div className="col">
                   
                   {/* <a className='form-control' id="instruction" href="#">Use account and password to continue</a> */}
                    <input type="text" className="form-control" id="instruction" placeholder="Use account and password to continue"  />
                    </div>
				
			</div>        	
        </div>
        <div className="form-group">
            <label for="email" className="name-lable" id="email" >Email</label>
        	<input type="email" className="form-control" name="email" onChange={(e)=>setEmail(e.target.value)} placeholder="Email" required="required" />
        </div>
		<div class="form-group">
            <label for="password" className="name-lable" id="password" >Password <a href="#">Forgot password?</a></label>
            <input type="password" className="form-control" name="password" onChange={(e)=>setPassword(e.target.value)} placeholder="Password" required="required" />
        </div>
		       
    
		<div class="form-group">
            <button type="submit" onClick={handleLogin  } className="btn btn-primary btn-lg btn-block ">Continue</button>
          
           
        </div>
   </div>

		
   
</div>
</div>
)

}

export default Login;